<?php
//-- configurazione
$DIR = '.';
$INCLUDE_SUB_DIRS = false;
//---
function fileSizeConvert($bytes){
    $bytes = floatval($bytes);
    $arBytes = array(
        array('TB',pow(1024,4)),
        array('GB',pow(1024,3)),
        array('MB',pow(1024,2)),
        array('KB',1024),
        array('B',1),
    );
    foreach($arBytes as $arItem){
        if($bytes >= $arItem[1]){
            $result = $bytes / $arItem[1];
            $result = str_replace('.',',',strval(round($result, 2))).' '.$arItem[0];
            break;
        }
    }
    return $result;
}

function getFileList($dir){
    global $INCLUDE_SUB_DIRS;
    // array to hold return value
    $retval = array();
    // add trailing slash if missing
    if(substr($dir, -1) != "/") $dir .= "/";
    // open pointer to directory and read list of files
    $d = @dir($dir) or die("getFileList: Failed opening directory $dir for reading");
    while(false !== ($entry = $d->read())) {
      // skip hidden files
      if($entry[0] == ".") continue;
      if(is_dir("$dir$entry")) {
          if($INCLUDE_SUB_DIRS){
              $retval[] = array(
                  "name" => "$entry/",
                  "type" => filetype("$dir$entry"),
                  "size" => 0,
                  "lastmod" => filemtime("$dir$entry")
              );   
          }
      } elseif(is_readable("$dir$entry")) {
        $retval[] = array(
          "name" => "$entry",
          "type" => mime_content_type("$dir$entry"),
          "size" => fileSizeConvert(filesize("$dir$entry")),
          "lastmod" => filemtime("$dir$entry")
        );
      }
    }
    $d->close();
    return $retval;
}
$DIR = rtrim($DIR,'/');
$dirlist = getFileList($DIR);
?>
<table>
    <thead>
        <tr><th>Nome</th><th>Tipo</th><th>Dimensione</th><th>Ultima modifica</th></tr>
    </thead>
    <tbody>
    <?php foreach($dirlist as $file) {
        echo "
        <tr>
            <td><a href=\"$DIR/$file[name]\" target=\"_blank\">$file[name]</a></td>
            <td>$file[type]</td><td>$file[size]</td>
            <td>".date('d/m/Y H:i',$file['lastmod'])."</td>
        </tr>";
    } ?>
    </tbody>
</table>