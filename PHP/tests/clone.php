<?php
/**
 * Test del comando 'clone' 
 */
class A{
    private $p1;
    protected $p2;
    public $p3;
    public $p4;
    public $p5;

    function __construct(){
        $this->p1 = 'private';
        $this->p2 = 'protected';
        $this->p3 = 'public';
        $this->p4 = 'original';
        $this->p5 = new stdClass;
        $this->p5->name = 'child';
    }
    
    //Metodo invocato in ogni istanza dell'oggetto clonata
    function __clone(){
        //$this->p4 = 'cloned';
        //$this->p5 = clone $this->p5;
        echo "Sono un'istanza clonata<br><br>";
    }
}

echo 'Istanza $a: <pre>';
$a = new A;

var_dump($a);
echo '</pre><br>';

echo 'Istanza $b: <pre>';
$b = clone $a;

var_dump($b);
echo '</pre><br>';
echo 'Sono uguali: ';
var_dump($a == $b);
echo '<br>';
echo 'Sono identiche: ';
var_dump($a === $b);
echo '<br>';