#include <pthread.h>
#include <stdio.h>

int l1,l2;

void *area(void *arg)
{
	int a;
    a = l1*l2;
	printf("area: %d\n",a);
	return NULL;
}

void *perimetro(void *arg)
{
	int p;
    p = l1*2+l2*2;
	printf("perimetro: %d\n",p);
	return NULL;
}

int main(void)
{
	pthread_t tID1;
	pthread_t tID2;
    printf("inserisci lato minore: ");
    scanf("%d",&l1);
    printf("inserisci lato maggiore: ");
    scanf("%d",&l2);
	pthread_create(&tID1, NULL, &area, NULL);
	pthread_create(&tID2, NULL, &perimetro, NULL);
	pthread_join(tID1, NULL);
	pthread_join(tID2, NULL);
	return 0;
} 