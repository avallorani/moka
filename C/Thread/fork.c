/* 
 */ 

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(){
    int i,pid,status,type;
    unsigned short nump;
    printf("Quanti processi figlio devo creare?\n");
    scanf("%hu",&nump);
    // Genera i 10 processi
    for(i=1;i<=nump;i++){
        pid = fork();
        if(pid==0){  // pid=0 -> figlio
            usleep(50000*i);  // Ritardo iniziale
            printf("Figlio: %d (padre %d)\n",i,getppid()); // Stampa messaggio del figlio
            usleep(500000*i);  // Ritardo finale
            if(i==3) abort();
            else exit(i);  // Termina con codice di ritorno
        } 
        else { // pid <> 0 -> padre (il pid e' quello del figlio)
            printf("Ho generato il figlio %d con pid %d\n",i,pid);
        }
    }

    // Attende che i processi terminino
    do{
        pid = wait(&status); // Attende termine di un figlio (uno qualunque)
        if(pid>0){
            if(WIFEXITED(status)){
                printf("Terminazione regolare del figlio %d con codice di uscita: %d\n",pid,WEXITSTATUS(status)); 
            }
            else if(WIFSIGNALED(status)){
                printf("Terminazione anomala del figlio %d con codice errore: %d\n",pid,WTERMSIG(status));
            }
        } 
        else printf("Tutti i figli hanno terminato!\n");
    }while(pid>0);
    return(0);
}