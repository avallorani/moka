#include <pthread.h>
#include <stdio.h>
void * tf1(void *arg)
{
	printf("x");
	printf("y");
	printf("z");
	return NULL;
}

void * tf2(void *arg)
{
	printf("a");
	printf("b");
	printf("c");
	return NULL;
}

int main(void)
{
	pthread_t tID1;
	pthread_t tID2;
	pthread_create(&tID1, NULL, &tf1, NULL);
	pthread_create(&tID2, NULL, &tf2, NULL);
	pthread_join(tID1, NULL);
	pthread_join(tID2, NULL);
	printf("\nfine\n");
	return 0;
} 