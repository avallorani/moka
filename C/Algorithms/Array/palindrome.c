//
//  CONTROLLA SE UNA PAROLA DATA E' PALINDROMA
//
//	@version 1.0.0 30/04/2014 19:26
//  @author Andrea Vallorani

#include <stdio.h>
#include <string.h>

int main(int argc, const char * argv[])
{
    char s[100];
    int i=1;
    int error=0;
    int n;
    
    printf("Inserisci parola da controllare: ");
    scanf("%s",s);
    
    printf("\n** Eseguo controllo **\n\n");
    n = strlen(s);
    do{
    	if(s[i-1]!=s[n-i]) error=1;
    	i++;
    }while(i<=(n/2) && !error);
    
    if(error) printf("NO");
    else printf("SI");
    
    printf("\n\n");
    
}