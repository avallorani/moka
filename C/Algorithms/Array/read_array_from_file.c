//
//  LEGGE UN ARRAY DA FILE
//
//	@version 1.0.0 21/05/2014 10:43
//  @author Andrea Vallorani

#include<stdlib.h>
#include <stdio.h>

void read_array_from_file(int vett[],int tot,char filename[]){
	FILE *fd;
	int i=-1;

	fd=fopen(filename,"r");
	//verifica eventuali errori di apertura
	if(fd==NULL){
    	perror("Errore in apertura del file");
    	exit(1);
	}
	// legge il numero di elementi del vettore
	do{
		i++;
		fscanf(fd,"%d",&vett[i]);
	}
	while(i<tot);
	fclose(fd);
}
