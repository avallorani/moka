/**
 * SCAMBIO DI VALORI TRA DUE VARIABILI DI TIPO CHAR
 *
 * @version 1.0.0 30/04/2014 18:52
 * @author Andrea Vallorani
 */
void swap_char(char *x,char *y)
{
    char temp;
    
    temp = *x;
    *x = *y;
    *y = temp;
}