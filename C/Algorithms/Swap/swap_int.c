/**
 * SCAMBIO DI VALORI TRA DUE VARIABILI DI TIPO INTEGER
 *
 * @version 1.0.0 29/09/2014 11:17
 * @author Andrea Vallorani
 */
void swap_int(int *x,int *y)
{
    int temp;
    
    temp = *x;
    *x = *y;
    *y = temp;  
}