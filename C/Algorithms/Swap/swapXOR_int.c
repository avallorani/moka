/**
 * SCAMBIO DI VALORI TRA DUE VARIABILI DI TIPO INTEGER
 * MEDIANTE L'USO DELL'OPERATORE XOR
 *
 * @version 1.0.0 30/12/2015 16:59
 * @author Andrea Vallorani
 */
void swapXOR_int(int *x,int *y)
{
    if(x!=y){
        *x ^= *y; // *x = *x ^ *y
        *y ^= *x;
        *x ^= *y;
    }
}