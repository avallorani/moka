//
//  RICERCA DICOTOMICA IN UN ARRAY
//
//	@version 1.0.0 12/05/2014 15:51
//  @author Andrea Vallorani

#include <stdio.h>
int main(int argc, char *argv[])
{
	int n = 10;
	int lista[10] = {1,5,15,18,25,29,30,40,45,49};
	int x;
	int trovato = -1;
    int p, u, m;
	
	printf("Inserisci numero intero da cercare: ");
    scanf("%d",&x);

    p = 0;
    u = n - 1;
    while(p <= u && trovato<0){
        m = (p + u) / 2;
        if(lista[m] == x) trovato = m;
        else{
        	if(lista[m] < x) p = m + 1;
        	else u = m - 1;
        }
    }
    
    if(trovato>=0) printf("Elemento trovato in posizione %d: ",trovato);
    else printf("Elemento non trovato");
    printf("\n");

}
		