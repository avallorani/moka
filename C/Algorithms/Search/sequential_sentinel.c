//
//  RICERCA SEQUENZIALE IN UN ARRAY CON SENTINELLA
//
//	@version 1.0.0 08/05/2014 17:27
//  @author Andrea Vallorani

#include <stdio.h>
int main(int argc, char *argv[])
{
     int i=0;
     int n=10;
     int trovato=0;
     int insieme[10] = {1,2,3,4,15,6,7,8,9,10};
     int x;

     printf("Inserisci numero intero da cercare: ");
     scanf("%d",&x);
     
 	 insieme[n] = x;
     while(!trovato){
        if(insieme[i]==x) trovato=1;
	    i++;
     }
     if(i<n) printf("Elemento trovato in posizione %d",i);
     else printf("Elemento non trovato");
     printf("\n");
}
		