//
// ORDINAMENTO SELECTION SORT
// L'ordinamento per selezione (selection sort) è un algoritmo di ordinamento che opera in place. 
// L'algoritmo è di tipo non adattivo, ossia il suo tempo di esecuzione non dipende dall'input ma dalla dimensione dell'array.
// L'algoritmo seleziona di volta in volta il numero minore nella sequenza di partenza e lo sposta nella sequenza ordinata; 
// di fatto la sequenza viene suddivisa in due parti: la sottosequenza ordinata, che occupa le prime posizioni dell'array, 
// e la sottosequenza da ordinare, che costituisce la parte restante dell'array.
// La complessità di tale algoritmo è dell'ordine di O(n^2)
//
// Nonostante l'approccio brutale adottato, ordinamento per selezione ha un'importante applicazione: poiché ciascun elemento 
// viene spostato al più una volta, questo tipo di ordinamento è il metodo da preferire quando si devono ordinare file 
// costituiti da record estremamente grandi e da chiavi molto piccole. Per queste applicazioni il costo dello spostamento 
// dei dati è prevalente sul costo dei confronti e nessun algoritmo è in grado di ordinare un file con spostamenti di dati 
// sostanzialmente inferiori a quelli dell'ordinamento per selezione.
//
// @version 1.0.0 16/05/2014 15:39
// @author Andrea Vallorani

#include <stdio.h>
#include <time.h>
#include "read_array_from_file.h"

int main(int argc, char *argv[])
{
	int n=99;
	int numeri[100];
	int temp,i,j,min,p=0,s=0;
	float ms;
    clock_t start, end;
     
	read_array_from_file(numeri,n,"ordinamento.txt");
     
	//printf("Ordino elementi con selectionsort ...\n");

    start = clock();   	
	for(i=0;i<n-1;i++){
		min = i;
		for(j=i+1;j<n;j++){
			if(numeri[j]<numeri[min]) min = j;
			p++;
		}
		if(i!=min){
			temp=numeri[min];
			numeri[min]=numeri[i];
			numeri[i]=temp;
			s++;
		}
	}
	end = clock();   
	for(i=0;i<n;i++){
		printf("%d ",numeri[i]);
	}
	ms = (((float)end - (float)start) / CLOCKS_PER_SEC ) * 1000;
	printf("\nOrdinato in %f ms (%d confronti e %d scambi)\n\n",ms,p,s);
}