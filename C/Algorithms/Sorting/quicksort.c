//
// ORDINAMENTO QUICK SORT
// Il merge sort è un algoritmo di ordinamento basato su confronti che utilizza un processo 
// di risoluzione ricorsivo, sfruttando la tecnica del Divide et Impera, che consiste nella 
// suddivisione del problema in sottoproblemi della stessa natura di dimensione via via più 
// piccola. Fu inventato da John von Neumann nel 1945.
//
//  1. Se la sequenza da ordinare ha lunghezza 0 oppure 1, è già ordinata. 
//  2. La sequenza viene divisa (divide) in due metà (se la sequenza contiene un numero dispari 
//     di elementi, la prima avrà un elemento in più della seconda)
//  3. Ognuna di queste sottosequenze viene ordinata, applicando ricorsivamente l'algoritmo (impera)
//     Le due sottosequenze ordinate vengono fuse (combina). Per fare questo, si estrae ripetutamente 
//     il minimo delle due sottosequenze e lo si pone nella sequenza in uscita, che risulterà ordinata
//
// L'algoritmo Merge Sort, per ordinare una sequenza di n oggetti, ha complessità O(n log n) sia nel 
// caso medio che nel caso pessimo. 
//
// @version 1.0.0 21/05/2014 12:31
// @author Andrea Vallorani

#include <stdio.h>
#include <time.h>
#include "read_array_from_file.h"

void mergesort(int[], int, int);
void merge(int[], int, int, int);

int main(int argc, char *argv[])
{
	int n=99;
	int numeri[100];
	int i;
	float ms;
    clock_t start, end;
     
	read_array_from_file(numeri,n,"ordinamento.txt");

    start = clock();   	
	mergesort(numeri,0,n-1);
	end = clock();   
	for(i=0;i<n;i++){
		printf("%d ",numeri[i]);
	}
	ms = (((float)end - (float)start) / CLOCKS_PER_SEC ) * 1000;
	printf("\nOrdinato in %f ms\n\n",ms);
}
