/**
 * ORDINAMENTO BUBBLE SORT
 * In informatica il Bubble Sort (letteralmente: ordinamento a bolle) è un semplice algoritmo di ordinamento dei dati. 
 * Il suo funzionamento è semplice: ogni coppia di elementi adiacenti della lista viene comparata e se essi sono nell'ordine sbagliato 
 * vengono invertiti. L'algoritmo scorre poi tutta la lista finché non vengono più eseguiti scambi, situazione che indica che la lista è ordinata.
 * L'algoritmo deve il suo nome al modo in cui gli elementi vengono ordinati, con quelli più piccoli che "risalgono" verso le loro posizioni 
 * corrette all'interno della lista così come fanno le bollicine in un bicchiere di spumante. In particolare, alcuni elementi attraversano 
 * la lista velocemente, altri più lentamente: i primi sono in gergo detti "conigli" e sono gli elementi che vengono spostati nella stessa 
 * direzione in cui scorre l'indice dell'algoritmo, mentre i secondi sono detti "tartarughe" e sono gli elementi che vengono spostati in 
 * direzione opposta a quella dell'indice.
 *
 * Come tutti gli algoritmi di ordinamento, può essere usato per ordinare dati di un qualsiasi tipo su cui sia definita una relazione d'ordine.
 *
 * Il Bubble sort non è un algoritmo efficiente: ha una complessità computazionale dell'ordine di O(n^2) confronti, con n elementi da ordinare; 
 * si usa solamente a scopo didattico in virtù della sua semplicità, e per introdurre i futuri programmatori al ragionamento algoritmico e alle 
 * misure di complessità.
 *
 * Il bubble sort effettua all'incirca {N^2}/{2} confronti ed {N^2}/{2} scambi sia in media che nel caso peggiore. 
 * Il tempo di esecuzione dell'algoritmo è O(n^2).
 *
 * @version 1.0.0 14/05/2014 18:43
 * @author Andrea Vallorani
 */
void bubblesort_int(int *numbers,int n,int *n_compare,int *n_swap)
{
    int swap = 1, height = n, i, temp;
    *n_swap = 0;
    *n_compare = 0;
    
    while(swap){
        swap = 0;
        height--;
        for(i=0; i<height; i++){
            if(numbers[i]>numbers[i+1]){
                temp = numbers[i];
                numbers[i] = numbers[i+1];
                numbers[i+1] = temp;
                //swap_int();
                swap = 1;
                (*n_swap)++;
            }
            (*n_compare)++;
        }
    }
}