//
// ORDINAMENTO SHAKER SORT
// Lo shaker sort è sostanzialmente una variante del bubble sort: si differenzia da quest'ultimo 
// per l'indice del ciclo più interno che, anziché scorrere dall'inizio alla fine, inverte la sua 
// direzione ad ogni ciclo. Pur mantenendo la stessa complessità, ovvero O(n²), lo shaker sort 
// riduce la probabilità che l'ordinamento abbia un costo corrispondente al caso peggiore.
//
// Noto anche come Bubble sort bidirezionale, Cocktail sort, Cocktail shaker sort, Ripple sort, 
// Happy hour sort o Shuttle sort è un algoritmo di ordinamento dei dati sviluppato dalla Sun Microsystems.
//
// Il nome shaker sort (ordinamento "a shaker", con riferimento allo strumento per preparare i cocktail) 
// suggerisce abbastanza chiaramente in cosa lo shaker sort modifichi il bubble sort. Anziché scorrere 
// l'array sempre nello stesso verso (privilegiando quindi gli spostamenti di valori in quel verso), 
// lo shaker sort semplicemente alterna una scansione in avanti e una all'indietro.
//
// Tutte le ottimizzazioni e le varianti previste per il bubble sort sono applicabili, con i dovuti adattamenti, 
// anche allo shaker sort.
//
// @version 1.0.0 15/05/2014 19:20
// @author Andrea Vallorani

#include <stdio.h>
#include <time.h>
#include "read_array_from_file.h"

int main(int argc, char *argv[])
{
	int n=99;
	int numeri[100];
	int scambio,temp,i,begin,max,p=0,s=0;
	float ms;
    clock_t start, end;
     
	read_array_from_file(numeri,n,"ordinamento.txt");
     
	//printf("Ordino elementi con shakersort ...\n");
	
    start = clock();   
	begin=-1;
	max=n-1;
	do{
		scambio=0;
		begin++;
		for(i=begin;i<max;i++){
			if(numeri[i]>numeri[i+1]){
				temp=numeri[i];
				numeri[i]=numeri[i+1];
				numeri[i+1]=temp;
				scambio=1;
				s++;
			}
			p++;
		}
		if(scambio){
			scambio=0;
			max--;
			for(i=max-1;i>=begin;i--){
				if(numeri[i]>numeri[i+1]){
					temp=numeri[i];
					numeri[i]=numeri[i+1];
					numeri[i+1]=temp;
					scambio=1;
					s++;
				}
				p++;
			}
		}
	}while(scambio);
 	end = clock();   
	for(i=0;i<n;i++){
		printf("%d ",numeri[i]);
	}
	ms = (((float)end - (float)start) / CLOCKS_PER_SEC ) * 1000;
	printf("\nOrdinato in %f ms (%d confronti e %d scambi)\n\n",ms,p,s);
}