/**
 * ORDINAMENTO INSERTION SORT
 * L'Insertion sort è un algoritmo relativamente semplice per ordinare un array che opera in place. 
 * Non è molto diverso dal modo in cui un essere umano, spesso, ordina un mazzo di carte. 
 * Pur essendo molto meno efficiente di algoritmi più avanzati, può avere alcuni vantaggi: ad esempio, 
 * è semplice da implementare ed è efficiente per insiemi di partenza che sono quasi ordinati.
 *
 * Il caso ottimo per l'algoritmo è quello in cui la sequenza di partenza sia già ordinata. In questo caso, 
 * l'algoritmo ha tempo di esecuzione lineare, ossia O(n). Infatti, in questo caso, in ogni iterazione il 
 * primo elemento della sottosequenza non ordinata viene confrontato solo con l'ultimo della sottosequenza 
 * ordinata. Il caso pessimo è invece quello in cui la sequenza di partenza sia ordinata al contrario. 
 * In questo caso, ogni iterazione dovrà scorrere e spostare ogni elemento della sottosequenza ordinata prima 
 * di poter inserire il primo elemento della sottosequenza non ordinata. Pertanto, in questo caso l'algoritmo 
 * ha complessità temporale quadratica, ossia O(n^2). Anche il caso medio ha complessità quadratica, il che 
 * lo rende impraticabile per ordinare sequenze grandi. 
 *
 * Risulta essere l'algoritmo di ordinamento più veloce per array piccoli. 
 *
 * @version 1.0.0 16/05/2014 15:39
 * @author Andrea Vallorani
 */
void insertionsort_int(int *numbers,int n,int *n_compare,int *n_swap)
{
    int next,i,j,made;
    *n_swap = 0;
    *n_compare = 0;
    
    for(i=1;i<n;i++){
        next = numbers[i];
        j = i;
        (*n_compare)++;
        made = 0;
        while(j>0 && next<numbers[j-1]){
            numbers[j] = numbers[j-1];
            j--;
            (*n_compare)++;
            made = 1;
            (*n_swap)++;
        }
        numbers[j] = next;
    }
}