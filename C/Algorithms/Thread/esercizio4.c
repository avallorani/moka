/**
* Conta ripetizioni elementi
**/
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int controlla(char *vettore,int n);
	
int main(int argc, const char *argv[]){
	
	int n;
	char caratteri[n];
	int i;
	
	sscanf(argv[1],"%d",&n);
	srand(time(NULL));
	for(i=0;i<n;i++){
		caratteri[i] = rand() % 10;
		printf("%d ",caratteri[i]);
	}
	if(controlla(caratteri,n)){
		printf("\nesito: OK\n");
	}
	else{
		printf("\nesito: NO\n");
	}
}

int controlla(char *vettore,int n){
	int i=0,j,tot;
	char current;
	int max=3;
	int result=0;
	while(i<=n-max && !result){
		current=vettore[i];
		tot=1;
		j=i+1;
		while(j<n && tot<=max){
			if(current==vettore[j]) tot++;
			j++;
		}
		i++;
		if(tot>max) result=1;
	}
	return result;
}