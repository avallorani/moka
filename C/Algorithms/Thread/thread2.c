#include <pthread.h>
#include <stdio.h>
//
int contoA=100, contoB=200;
int totale;
//
void * trasferisci(void *arg)
{
	int importo=*((int*)arg);
	int cA,cB;
	//inizio sequenza critica
	cA = contoA; //leggi contoA in variabile locale
	cB = contoB; //leggi contoB in variabile locale
	contoA = cA + importo;
	contoB = cB - importo;
	//fine sequenza critica
	return NULL;
}
//
int main(void)
{
	pthread_t tID1;
	pthread_t tID2;
	int importo1=10, importo2=20;
	pthread_create(&tID1, NULL, &trasferisci, &importo1);
	pthread_create(&tID2, NULL, &trasferisci, &importo2);
	pthread_join(tID1, NULL);
	pthread_join(tID2, NULL);
	totale=contoA + contoB;
	printf("contoA = %d contoB = %d\n", contoA, contoB);
	printf("il totale è %d\n", totale);
	return 0;
} 