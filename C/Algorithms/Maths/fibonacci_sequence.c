//
//  STAMPA DEI PRIMI N NUMERI DELLA SUCCESSIONE DI FIBONACCI
//
//	@version 1.0.0 30/04/2014 18:55
//  @author Andrea Vallorani

#include <stdio.h>

int main(int argc, const char * argv[])
{
    const int MAX=60;
    long int secondo = 1, primo = 1, prossimo;
    int tot, i;
    do{
        printf("Quanti numeri vuoi stampare? ");
        scanf("%d",&tot);
        if(tot>MAX){
            printf("Il numero massimo supportato è %d\n",MAX);
        }
    }
    while(tot>MAX);
    printf("\nFIBONACCI (primi %d numeri): %ld %ld ",tot,primo,secondo);
        
    for( i = 2 ; i < tot ; i++){
        prossimo = secondo + primo;
        primo = secondo;
        secondo = prossimo;
        printf("%ld ", prossimo);
    }
    
    printf("\n\n");
}

