//
//  CONTROLLA SE UN NUMERO DATO APPARTIENE ALLA SUCCESSIONE DI FIBONACCI
//
//	@version 1.0.0 30/04/2014 18:55
//  @author Andrea Vallorani

#include <stdio.h>
#include <math.h>

int main(int argc, const char * argv[])
{
    long int numero,q,r;
    printf("Inserisci numero da controllare: ");
    scanf("%ld",&numero);
    printf("\n** ESEGUO VERIFICA **\n\n");
    q = numero * numero * 5 - 4;
    r = sqrt(q);
    if(q%r){
	    q = numero * numero * 5 + 4;
    	r = sqrt(q);
    	if(q%r) printf("NO");
    	else printf("SI");
    }
    else printf("SI");
    
    printf("\n\n");
}

