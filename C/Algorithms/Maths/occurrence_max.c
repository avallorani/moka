//
//  DATO UN ARRAY DI NUMERI INTERI, CALCOLA I NUMERI CHE SI RIPETONO CON MAGGIOR FREQUENZA
//
//	@version 1.0.0 30/04/2014 18:52
//  @author Andrea Vallorani

#include <stdio.h>

int main(int argc, const char * argv[])
{
    int n=10;
    int numeri[n];
    int i,j,tot,n_max;
    int max=0;
    int numeri_max[n];
    
    for(i=0;i<n;i++){
    	printf("Inserisci numero intero %d: ",(i+1));
    	scanf("%d",&numeri[i]);
    }
    
    printf("\n** Eseguo controllo **\n\n");
    
    for(i=0;i<n;i++){
    	tot=1;
    	for(j=i+1;j<n;j++){
    		if(numeri[i]==numeri[j]) tot++;
    	}
    	if(tot>=max){
    		if(tot>max){
    			max=tot;
    			n_max=0;
    		}
    		numeri_max[n_max]=numeri[i];
    		n_max++;
    	}
    }
    
    printf("I numeri che si ripetono maggiormente (%d volte):",max);
    for(i=0;i<n_max;i++){
    	printf(" %d",numeri_max[i]);
    }
    
    printf("\n\n");
}

