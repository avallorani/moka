//
//  ALGORITMO PER IL CALCOLO DEL NUMERO MASSIMO
//
//	@version 1.0.0 30/04/2014 18:52
//  @author Andrea Vallorani

#include <stdio.h>

int main(int argc, const char * argv[])
{
    int numeri[5];
    int i;
    int max;
    
    for(i=0;i<5;i++){
    	printf("Inserisci numero intero %d: ",(i+1));
    	scanf("%d",&numeri[i]);
    }
    
    printf("\n** Eseguo controllo **\n\n");
    
    max=numeri[0];
    for(i=1;i<5;i++){
    	if(numeri[i]>max) max=numeri[i];
    }
    
    printf("Il numero massimo è: %d\n\n",max);
    
}

//
//  EQUAZIONE 1° GRADO
//
//	@version 1.0.0 05/05/2015 13:43
//  @author Andrea Vallorani

#include <stdio.h>
int main(int argc, const char * argv[])
{
    float a;
    float b;
    float x;
    
	printf("Inserisci il valore di a: ");
	scanf("%f",&a);
	printf("Inserisci il valore di b: ");
	scanf("%f",&b);
    
    printf("\n** Calcolo risultato **\n\n");
    
    if(a==0){
        if(b==0){
            printf("Equazione impossibile\n\n");
        }
        else{
            printf("Equazione indeterminata\n\n");
        }
    }
    else{
        x = b/a;
        printf("x = %f",x);
    }  
    printf("\n\n");
}

