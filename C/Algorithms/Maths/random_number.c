//
//  RICERCA SEQUENZIALE IN UN ARRAY CON SENTINELLA
//
//	@version 1.0.0 08/05/2014 17:27
//  @author Andrea Vallorani

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int rand_lim(int limit);

int main(int argc, char *argv[])
{
	const int MAX_TENTATIVI = 5;
	int r,n;
	int tentativi=0;
	int indovinato=0;

	srand(time(NULL));
	r=rand()%50;
	//r=rand_lim(50);
	printf("\nIl computer ha pensato un numero da 0 a 50. Prova ad indovinarlo. Hai a disposizione %d tentativi.\n",MAX_TENTATIVI);
     
	do{
		tentativi++;
		printf("Tentativo %d: ",tentativi);
		scanf("%d",&n);
		if(r==n){
			indovinato=1;
			printf("Hai indovinato !!");
		}
		else printf(":(\n");
		
	}while(!indovinato && tentativi<MAX_TENTATIVI);
    
    if(!indovinato){
    	printf("\nIl numero pensato era %d",r);
    }
	printf("\n");     
}

int rand_lim(int limit){
    int divisor = RAND_MAX/(limit+1);
    int retval;

    do { 
        retval = rand() / divisor;
    } while (retval > limit);

    return retval;
}