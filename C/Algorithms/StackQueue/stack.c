//
// IMPLEMENTAZIONE DI UNO STACK TRAMITE ARRAY
//
// @version 1.0.0 29/09/2014 11:17
// @author Andrea Vallorani

#include <stdio.h>

typedef struct{
  int elements[10];
  int top;
  int size;
}stack;

void push(stack *s,int element);
int pop(stack *s,int *element);
int isEmpty(stack *s);
int isFull(stack *s);
void printStack(stack *s);

int main(int argc, const char * argv[])
{
    stack myStack;
	int el,i;
	char ch;
	myStack.top=-1;
	myStack.size=10;
	printf("----\nIMPLEMENTAZIONE DI UNO STACK TRAMITE ARRAY\nauthor: Andrea Vallorani\n\nComandi disponibili:\n1-Inserisci elemento (push)   2-Togli elemento (pop)   3-Stampa stack   x-Esci\n\n");
	do{
		scanf("%c",&ch);
		switch(ch){
			case '1':
				printf("Inserisci numero da caricare nello stack: ");
				scanf("%d",&el);
				push(&myStack,el);
			break;
			case '2':
				pop(&myStack,&el);
			break;
			case '3':
				printStack(&myStack);
			break;
		}
	}while(ch!='x');
}

void push(stack *s,int element){
	if(!isFull(s)){
		s->elements[++s->top]=element;
		printf("Inserito elemento %d in posizione %d\n",element,s->top);
	}
	else{
		printf("Stack pieno (evitato overflow)!\n");
	}
}

int pop(stack *s,int *element){
	if(!isEmpty(s)){
		*element = s->elements[s->top];
		printf("Eliminato dalla posizione %d l'elemento %d\n",s->top,*element);
		s->top--;
		return 1;
	}
	else{
		printf("Stack vuoto (evitato underflow)!\n");
		return 0;
	}
}

int isEmpty(stack *s){
	return s->top < 0;
}

int isFull(stack *s){
	return s->top+1 == s->size;
}

void printStack(stack *s){
	int i;
	printf("-");
	for(i=0;i<s->size;i++){
		printf("---");
	}
	printf("\n|");
	for(i=0;i<s->size;i++){
		if(i<=s->top) printf("%d|",s->elements[i]);
		else printf("  |");
	}
	printf("\n-");
	for(i=0;i<s->size;i++){
		printf("---");
	}
	printf("\n\n");
}

