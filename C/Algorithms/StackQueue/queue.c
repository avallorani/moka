//
// IMPLEMENTAZIONE DI UNA CODA TRAMITE ARRAY
//
// @version 1.0.0 29/09/2014 16:43
// @author Andrea Vallorani

#include <stdio.h>

typedef struct{
  int elements[10];
  int head;
  int tail;
  int size;
}queue;

void enqueue(queue *q,int element);
int dequeue(queue *q,int *element);
int isEmpty(queue *q);
int isFull(queue *q);
void printQueue(queue *q);

int main(int argc, const char * argv[])
{
    queue myQueue;
	int el,i;
	char ch;
	myQueue.head=0;
	myQueue.tail=0;
	myQueue.size=10;
	printf("----\nIMPLEMENTAZIONE DI UNA CODA TRAMITE ARRAY\nauthor: Andrea Vallorani\n\nComandi disponibili:\n1-Inserisci elemento (enqueue)  2-Togli elemento (dequeue)  3-Stampa coda   x-Esci\n\n");
	do{
		scanf("%c",&ch);
		switch(ch){
			case '1':
				printf("Inserisci numero da caricare nella coda: ");
				scanf("%d",&el);
				enqueue(&myQueue,el);
			break;
			case '2':
				dequeue(&myQueue,&el);
			break;
			case '3':
				printQueue(&myQueue);
			break;
		}
	}while(ch!='x');
}

void enqueue(queue *q,int element){
	if(!isFull(q)){
		q->elements[q->tail]=element;
		printf("Inserito elemento %d in posizione %d\n",element,q->tail);
		q->tail=(q->tail+1)%q->size;
	}
	else{
		printf("Coda piena, non si possono inserire ulteriori elementi!\n");
	}
}

int dequeue(queue *q,int *element){
	if(!isEmpty(q)){
		*element = q->elements[q->head];
		printf("Eliminato dalla posizione %d l'elemento %d\n",q->head,*element);
		q->head=(q->head+1)%q->size;
		return 1;
	}
	else{
		printf("Coda vuota, nessun elemento presente!\n");
		return 0;
	}
}

int isEmpty(queue *q){
	return q->head == q->tail;
}

int isFull(queue *q){
	return q->head == ((q->tail+1)%q->size);
}

void printQueue(queue *q){
	int i;
	printf("-");
	for(i=0;i<q->size;i++){
		printf("---");
	}
	printf("\n|");
	for(i=0;i<q->size;i++){
		if( (q->head<q->tail && i>=q->head && i<q->tail) || (q->head>q->tail && (i>=q->head || i<q->tail)) ) printf("%d|",q->elements[i]);
		else printf("  |");
	}
	printf("\n-");
	for(i=0;i<q->size;i++){
		printf("---");
	}
	printf("\n\n");
}

