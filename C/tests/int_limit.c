#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/**
 * VERIFICA LIMITE NUMERI INTERI
 * Autore: Andrea Vallorani
 * Data: 17/11/2016
 */
 int main(){
     
     printf("INT\n");
     int a=0;
     int a2=1;
     int pot;
     while(a2>a){
         a++;
         a2++;
     }
     printf("max: %d\n",a);
     printf("min: %d\n",a2);
     pot = log2(a);
     printf("(%d bit)\n",pot+2);
     printf("---\n");
     
     printf("SHORT INT\n");
     short int b=0;
     short int b2=1;
     while(b2>b){
         //printf("\r%hd",b);
         b++;
         b2++;
     }
     printf("max: %d\n",b);
     printf("min: %d\n",b2);
     pot = log2(b);
     printf("(%d bit)\n",pot+2);
     printf("---\n");
     
     printf("UNSIGNED INT\n");
     unsigned int c=0;
     unsigned int c2=1;
     while(c2>c){
         //printf("\r%hd",b);
         c++;
         c2++;
     }
     printf("max: %u\n",c);
     printf("min: %u\n",c2);
     pot = log2(c);
     printf("(%d bit)\n",pot+1);
     printf("---\n");
 }