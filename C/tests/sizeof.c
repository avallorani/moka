#include <stdio.h>

/**
 * MISURATORE DIMENSIONI
 * Mostra la dimensione dei tipi base del linguaggio C
 * Autore: Andrea Vallorani
 * Versione: 1.0
 * Data: 17/11/2016
 */
 int main(){
     printf("char:\t\t\t %lu bytes\n",sizeof(char));
     printf("short int:\t\t %lu bytes\n",sizeof(short int));
     printf("short unsigned int:\t %lu bytes\n",sizeof(short unsigned int));
     printf("int:\t\t\t %lu bytes\n",sizeof(int));
     printf("unsigned int:\t\t %lu bytes\n",sizeof(unsigned int));
     printf("float:\t\t\t %lu bytes\n",sizeof(float));
     printf("double:\t\t\t %lu bytes\n",sizeof(double));
     printf("long int:\t\t %lu bytes\n",sizeof(long int));
     printf("long unsigned int:\t %lu bytes\n",sizeof(long unsigned int));
     printf("long long int:\t\t %lu bytes\n",sizeof(long long int));
     printf("long double:\t\t %lu bytes\n",sizeof(long double));
     return(0);
 }