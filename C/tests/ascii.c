#include <stdio.h>

/**
 * TABELLA ASCII STANDARD
 * Stampa la tabella ASCII standard
 * Autore: Andrea Vallorani
 * Versione: 1.0
 * Data: 19/11/2016
 */
 int main(){
     const short MAX = 128;
     unsigned short i;
     printf("Dec | Char\n---   ----\n");
     for(i=0;i<MAX;i++){
        printf("%3d | %c\n",i,i);
     }
     return(0);
 }