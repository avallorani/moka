/**
 * Interfaccia da riga di comando per testare gli 
 * algoritmi di swap
 *
 * @version 1.0.0 30/12/2015 13:35
 * @author Andrea Vallorani
 */
#include <stdio.h>
#include "../../Algorithms/Swap/swap.h"
    
int main()
{
    int x,y;
	int c;
    
    do{
    	printf("Inserisci numero intero X: ");
    	c = scanf("%d",&x);
    	if (c == 0) {
	    	scanf("%*[^\n]");
			printf("Attenzione: input non valido.\n");
		}
    }while(c==0);    
    do{
    	printf("Inserisci numero intero Y: ");
    	c = scanf("%d",&y);
    	if (c == 0) {
	    	scanf("%*[^\n]");
			printf("Attenzione: input non valido.\n");
		}
    }while(c==0);
    
    printf("\nX(%p): %d  -  Y(%p): %d",&x,x,&y,y);
    
    printf("\n\nEseguo scambio <->\n");
    swap_int(&x,&y);
    printf("\nX(%p): %d  -  Y(%p): %d",&x,x,&y,y);
    
    printf("\n\nEseguo scambio XOR <->\n");
    swapXOR_int(&x,&y);
    printf("\nX(%p): %d  -  Y(%p): %d",&x,x,&y,y);
    
	printf("\n\n");
}