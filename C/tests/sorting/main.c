#include <stdio.h>
#include <string.h>
#include <time.h>
#include "../../Algorithms/Array/read_array_from_file.h"
#include "../../Algorithms/Sorting/sorting.h"
#define NUM 15

int main(int argc, char *argv[])
{
    int n=NUM,numeri[NUM],numery_copy[NUM],i,c,s;
    float ms;
    clock_t start, end;
     
    read_array_from_file(numeri,n,"numbers.txt");
     
    printf("Ordino elementi con Bubble Sort ...\n");
    memcpy(numery_copy, numeri, NUM * sizeof(int));
    start = clock();
    bubblesort_int(numery_copy,n,&c,&s);
 	end = clock();
    for(i=0;i<n;i++){
    	printf("%d ",numery_copy[i]);
    }
    ms = (((float)end - (float)start) / CLOCKS_PER_SEC ) * 1000;
    printf("\nOrdinato in %f ms (%d confronti e %d scambi)\n\n",ms,c,s);
    
    
    printf("Ordino elementi con Insertion Sort ...\n");
    memcpy(numery_copy, numeri, NUM * sizeof(int));
    start = clock();
    insertionsort_int(numery_copy,n,&c,&s);
 	end = clock();
    for(i=0;i<n;i++){
    	printf("%d ",numery_copy[i]);
    }
    ms = (((float)end - (float)start) / CLOCKS_PER_SEC ) * 1000;
    printf("\nOrdinato in %f ms (%d confronti e %d scambi)\n\n",ms,c,s);
 }