package geometry;

public class Triangle {
    
    private Point a;
    private Point b;
    private Point c;
    private final double ERR_MAX = 0.00000001;
    
    public Triangle(Point a, Point b, Point c){
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public Triangle (Triangle t) {
        a = t.a;
        b = t.b;
        c = t.c;
    }
    
    public void setA(Point a) {
        this.a = a;
    }
    
    public void setB(Point b) {
        this.b = b;
    }
    
    public void setC(Point c) {
        this.c = c;
    }
    
    public Point getA(){
        return a;
    }
    
    public Point getB(){
        return b;
    }
    
    public Point getC(){
        return c;
    }
    
    public double perimeter(){
        double side1 = a.distance(b);
        double side2 = b.distance(c);
        double side3 = c.distance(a);
        return side1+side2+side3;
    }
    
    public double area(){
        double side1 = a.distance(b);
        double side2 = b.distance(c);
        double side3 = c.distance(a);
        double p = perimeter() / 2;
        return Math.sqrt(p*(p-side1)*(p-side2)*(p-side3));
    }
    
    public boolean equals(Triangle t) { 
        return ((a.equals(t.a)) && (b.equals(t.b)) && (c.equals(t.c)));
    }
    
    public boolean equivalent(Triangle t) {
        return (Math.abs(this.area()-t.area()) <= ERR_MAX);
    }
    
    public String toString() { 
        return "A"+a.toString()+" B"+b.toString()+" C"+c.toString();
    }
    
    public static void main(String[] args) {
        Point p1 = new Point(1,1);
        Point p2 = new Point(2,2);
        Point p3 = new Point(2,1);
        Point p4 = new Point(1,4);
        Point p5 = new Point(3.,3.);
        Triangle t1 = new Triangle(p1,p2,p3);
        Triangle t2 = new Triangle(p1,p3,p4);
        Triangle t3 = new Triangle(p1,p2,p2);
        Triangle t4 = new Triangle(t1);

        System.out.println("---");
        System.out.println("Triangolo T1: "+t1.toString());
        System.out.println("Perimetro T1: "+t1.perimeter());
        System.out.println("Area T1: "+t1.area());
        System.out.println("---");
        System.out.println("Triangolo T2: "+t2.toString());
        System.out.println("Perimetro T2: "+t2.perimeter());
        System.out.println("Area T2: "+t2.area());
        System.out.println("---");
        System.out.println("Triangolo T3: "+t3.toString());
        System.out.println("Perimetro T3: "+t3.perimeter());
        System.out.println("Area T3: "+t3.area());
        System.out.println("---");
        System.out.println("Triangolo T4: "+t4.toString());
        System.out.println("Perimetro T4: "+t4.perimeter());
        System.out.println("Area T4: "+t4.area());
        System.out.println("---");

        if (t1.equals(t4)) System.out.println("I triangoli T1 e T4 sono uguali");
        if (t1.equivalent(t4)) System.out.println("I triangoli T1 e T4 sono equivalenti");
        if (t1.equals(t2)) System.out.println("I triangoli T1 e T2 sono uguali");
        if (t1.equivalent(t2)) System.out.println("I triangoli T1 e T2 sono equivalenti");
    }
}