package geometry;

public class Point {
    
    /*x coordinate*/
    private double x;
    
    /*y coordinate*/
    private double y;
    
    public Point(double x, double y) {
        setX(x);
        setY(y);    
    }
    
    public Point(Point p) {
        setX(p.getX());
        setY(p.getY()); 
    }
    
    public void setX(double x) {
        this.x = x;
    }
    
    public void setY(double y) {
        this.y = y;
    }
    
    public double getX() {
        return x;
    }
    
    public double getY() {
        return y;
    }
    
    public double distance(Point p) { 
        double dx = x - p.getX();
        double dy = y - p.getY();
        return Math.sqrt((dx*dx)+(dy*dy));
    }
    
    public boolean equals(Point p) {
        return ((x==p.x) && (y==p.y));   
    }
    
    public String toString() { 
        return "("+x+";"+y+")"; 
    }
    
    public static void main(String[] args) {
        Point p1 = new Point(0.4,1.7);
        Point p2 = new Point(2,3.62);
        Point p3 = new Point(p1); 
        System.out.println("P1= "+p1.toString()); 
        System.out.println("P2= "+p2.toString());
        System.out.println("P3= "+p3.toString()); 
        System.out.println("Distanza P1-P2: "+p1.distance(p2));
        System.out.println("Distanza P1-P3: "+p1.distance(p3)); 
        if(p1.equals(p3)) {
            System.out.println("P1 e P3 coincidono");
        }
        else{ 
            System.out.println("P1 e P3 non coincidono");
        }
    }
}