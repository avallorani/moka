package moka.java.tests;
import java.util.*;

/**
 * Esempio di utilizzo dei package. Per eseguire il programma usare uno dei 2 comandi:
 *   java moka.java.tests.PackageTest
 *   java moka/java/tests/PackageTest
 * @version 1.0.0
 * @author Andrea Vallorani <andrea@vallorani.org>
 * @since 2016-07-14 18:26
 */
class PackageTest {

    public static void main(String[] args) {
        System.out.print("Ciao, sono una classe contenuta in un package!\n");
    }
}