import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.*;

public class SwingTest{

    public static void main(String[] args) {
        
        String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("mac")) {
            System.setProperty("apple.laf.useScreenMenuBar", "true");
            System.setProperty("com.apple.mrj.application.apple.menu.about.name", "Test");
        }
                
        //nuova finestra
        JFrame frame = new JFrame("Test");
        
        //imposta dimensioni
        frame.setSize(500, 200);
        //frame.pack();
        
        //centro nello schermo
        frame.setLocationRelativeTo(null);
        
        //comportamento all'uscita
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //contenuto del frame
        Container pane = frame.getContentPane();
        
        JPanel controls = new JPanel();
        controls.setLayout(new GridLayout(1,2));
        controls.add(new JLabel("Ciao a tutti"));
        controls.add(new JLabel("Ciao mondo"));
        
        //aggiungo una lista
        String[] data = {"black","blue","green","yellow","white"};
        JList<String> list = new JList<String>(data);
        list.setSelectedIndex(0);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        pane.add(controls, BorderLayout.NORTH);
        //pane.add(new JSeparator(), BorderLayout.CENTER);
        pane.add(new JScrollPane(list), BorderLayout.SOUTH);
        
        //menu del frame
        JMenuBar menuBar;
        JMenu menu, submenu;
        JMenuItem menuItem;

        menuBar = new JMenuBar();
        menu = new JMenu("A Menu");
        menu.setMnemonic(KeyEvent.VK_A);
        menu.getAccessibleContext().setAccessibleDescription(
                "The only menu in this program that has menu items");
        menuBar.add(menu);

        //a group of JMenuItems
        menuItem = new JMenuItem("A text-only menu item",
                KeyEvent.VK_T);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_1, ActionEvent.ALT_MASK));
        menuItem.getAccessibleContext().setAccessibleDescription(
                "This doesn't really do anything");
        menu.add(menuItem);

        menuItem = new JMenuItem("Both text and icon");
        menuItem.setMnemonic(KeyEvent.VK_B);
        menu.add(menuItem);

        //a submenu
        menu.addSeparator();
        submenu = new JMenu("A submenu");
        submenu.setMnemonic(KeyEvent.VK_S);

        menuItem = new JMenuItem("An item in the submenu");
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_2, ActionEvent.ALT_MASK));
        submenu.add(menuItem);

        menuItem = new JMenuItem("Another item");
        submenu.add(menuItem);
        menu.add(submenu);

        //Build second menu in the menu bar.
        menu = new JMenu("Another Menu");
        menu.setMnemonic(KeyEvent.VK_N);
        menu.getAccessibleContext().setAccessibleDescription(
                "This menu does nothing");
        menuBar.add(menu);
        frame.setJMenuBar(menuBar);
        
        //rendi visibile
        frame.setVisible(true);
    }
}