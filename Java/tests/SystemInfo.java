import java.util.*;

/**
 * Permette la visualizza di alcune risorse hardware del computer
 * @version 1.0.0
 * @author Andrea Vallorani <andrea@vallorani.org>
 * @since 2016-04-13 12:49
 */
class SystemInfo {

    public static void main(String[] args) {
        String name;
        Integer ch;
        Boolean noexit = true;
        Scanner sc = new Scanner(System.in);
        System.out.print("Inserisci il tuo nome: ");
        name = sc.nextLine();
        System.out.println("\nCiao " + name + " scegli cosa vuoi vedere:");
        System.out.println("\t1 - RAM");
        System.out.println("\t2 - Processore");
        System.out.println("\t3 - Versione java");
        System.out.println("\t0 - Esci");

        while (noexit) {
            ch = Integer.parseInt(sc.nextLine());
            switch (ch) {
                case 1:
                    Integer spaceT = (int) Runtime.getRuntime().totalMemory() / 1000000;
                    System.out.println("Spazio RAM totale: " + spaceT + "MB");
                    Integer space = (int) Runtime.getRuntime().freeMemory() / 1000000;
                    System.out.println("Spazio RAM disponibile: " + space + "MB");
                    break;
                case 2:
                    System.out.println("N. processori: " + Runtime.getRuntime().availableProcessors());
                    break;
                case 3:
                    System.out.println("JAVA " + System.getProperty("java.version"));
                    break;
                case 0:
                    noexit = false;
            }
        }
    }
}