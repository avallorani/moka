import java.util.*;

/**
 * Hello, world!
 * @version 1.0.0
 * @author Andrea Vallorani <andrea@vallorani.org>
 * @since 2016-07-13 18:57
 */
class HelloWorld {

    public static void main(String[] args) {
        System.out.print("Hello, World!\n");
    }
}