/**
 * Calcolo area rettangolo
 * @param {float} base
 * @param {float} height
 * @returns {float}
 * 
 * @version 1.0.0 07/01/16 12.00
 * @author Andrea Vallorani
 */
function rectangle_area(base,height){
    var area;
    /*base = prompt('Inserisci base:');
    base = parseInt(base);
    altezza = prompt('Inserisci altezza:');
    altezza = parseInt(altezza);*/
    area = base * height;
    return area;
};